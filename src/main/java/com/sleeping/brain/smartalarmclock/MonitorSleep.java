package com.sleeping.brain.smartalarmclock;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.PointF;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.os.Environment;
import android.os.IBinder;
import android.os.PowerManager;
import android.preference.PreferenceManager;
import android.widget.Toast;

import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by Micheal on 11/27/2015.
 */
public class MonitorSleep extends Service implements SensorEventListener{

    static final float NOISE = 0.5f;
    static final long TIME_INTERVAL = 1000;

    //Ranges (exclusive)
    static final int REM_MAX = 5 * 1000;
    static final int SEMI_REM_MIN = 4 * 1000;
    static final int SEMI_REM_MAX = 30 * 1000;
    static final int NON_REM_MIN = 29 * 1000;

    static final int REM = 2;
    static final int SEMI_REM = 1;
    static final int NON_REM = 0;
    private static final double SAFETY_MARGIN_PERCENT = 0.5d; //Percent of sleep cycle
    static final int START_SLEEP_TYPE = -1;

    private SensorManager mSensorManager;
    private Sensor mSensor;

    private PowerManager.WakeLock wl;
    Calendar calendar;
    MediaPlayer mp;
    long timeStartRecording = 0L;
    static int startHour = 0;
    static int startMinute = 0;
    int wakingHour = 0;
    int wakingMinute = 0;

    BufferedWriter output;

    public float lastX;
    public float lastY;
    public float lastZ;

    public float deltaX;
    public float deltaY;
    public float deltaZ;

    String date;
    boolean sleeping;
    long lastRecording;
    long startOfPhaseTime;
    boolean lessThanTwoHoursUntilWakeUp = false;
    int sleepType = START_SLEEP_TYPE;
    long lastSleepCycleDuration;
    int phaseIndex = 0;
    boolean finalSleepCycle = false;
    boolean wakie;
    NotificationManager notificationManager;

    //Types of sleep in sequence
    ArrayList<Integer> types = new ArrayList<Integer>();
    ArrayList<Long> durations = new ArrayList<Long>();
    //Durations, in sequenceof a certain sleep phase
    ArrayList<Long> remDurations = new ArrayList<Long>();
    ArrayList<Long> semiRemDurations = new ArrayList<Long>();
    ArrayList<Long> nonRemDurations = new ArrayList<Long>();
    //Average Duration
    float remAverage = 0L;
    float nonRemAverage = 0L;
    float semiRemAverage = 0L;
    //Total Duration
    Long remTotal = 0L;
    Long nonRemTotal = 0L;
    Long semiRemTotal = 0L;
    //Amount of occurances
    int remCounter = 0;
    int nonRemCounter = 0;
    int semiRemCounter = 0;
    //string that makes the graph
    ArrayList <PointF> graphPoints;
    float pointIndex = 0;



    //Timer thread
    Thread thread = new Thread()
    {
        boolean running = true;
        @Override
        public void run() {
            running = true  ;
            while (!Thread.currentThread().isInterrupted()) {

                    long time = System.currentTimeMillis();
                    //while(((System.currentTimeMillis()-time)<60*1000) || !Thread.currentThread().isInterrupted());
                try {
                    Thread.sleep(60*1000,0);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }


                notificationManager.cancelAll();
                Notification n  = new Notification.Builder(getApplicationContext())
                        .setContentTitle("Alarm Set")
                        .setContentText("Alarm at " + wakingHour + ":" + (wakingMinute <10?"0"+ wakingMinute : wakingMinute) + ". In " + timeUntilWake()/1000f/60f/60f + " hours.")
                        .setSmallIcon(R.drawable.ic_launcher_a)
                        .setAutoCancel(true).build();
                notificationManager.notify(1338, n);

            }
        }
        @Override
        public void interrupt () {
            super.interrupt();
            running = false;
        }

    };

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        return null;
    }
    @Override
    public void onCreate() {
        super.onCreate();
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // For time consuming an long tasks you can launch a new thread here...

        //retreive info from activity, if not there, retrieve from shared preferences
        wakingHour = intent.getIntExtra("WakeHour", PreferenceManager.getDefaultSharedPreferences(this).getInt("LastWakeHour", -1));
        wakingMinute = intent.getIntExtra("WakeMinute", PreferenceManager.getDefaultSharedPreferences(this).getInt("LastWakeMinute", 0));

        calendar = Calendar.getInstance();

        graphPoints = new ArrayList<>();

        notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        Notification n  = new Notification.Builder(this)
                .setContentTitle("Alarm Set")
                .setContentText("Alarm at " + wakingHour + ":" + (wakingMinute <10?"0"+ wakingMinute : wakingMinute) + ". In " + timeUntilWake()/1000f/60f/60f + " hours.")
                .setSmallIcon(R.drawable.ic_launcher_a)
                .setAutoCancel(true).build();
        startForeground(1337, n);

        date = (new SimpleDateFormat("dd-MMM-yyyy")).format(Calendar.getInstance().getTime());

        //Set up wakelock, can turn off screen
        PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
        wl = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                "MyWakelockTag");

        //Set up accelerometer
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mSensorManager.registerListener(this, mSensor, SensorManager.SENSOR_DELAY_NORMAL);

        calendar = Calendar.getInstance();

        Toast.makeText(getApplicationContext(), "Recording sleep.",
                Toast.LENGTH_LONG).show();

        recordSleep();

        //wakeUp();

        thread.start();

        return START_REDELIVER_INTENT;
    }
    @Override
    public void onDestroy() {
        stopForeground(true);
        stopRecordSleep();
        thread.interrupt();
//        thread.stop();
        super.onDestroy();
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType()==Sensor.TYPE_ACCELEROMETER) {

            deltaX = Math.abs(event.values[0] - lastX);
            deltaY = Math.abs(event.values[1] - lastY);
            deltaZ = Math.abs(event.values[2] - lastZ);

            lastX = event.values[0];
            lastY = event.values[1];
            lastZ = event.values[2];

            long timeDelta = System.currentTimeMillis() - lastRecording;
            if (sleeping && (deltaX > NOISE || deltaY > NOISE || deltaZ > NOISE) && (timeDelta) > TIME_INTERVAL) {
                lastRecording = System.currentTimeMillis();
                calendar = Calendar.getInstance();
                try {

                    if (isExternalStorageWritable()) {

                        output.write((System.currentTimeMillis() - timeStartRecording) + ";"  + calendar.get(Calendar.HOUR_OF_DAY) + ";"  + calendar.get(Calendar.MINUTE) + ";" + deltaX + ";" + deltaY + ";" + deltaZ + "\n");
                        output.newLine();
                        output.flush();
                        /*
                        //
                        if (sleepType != START_SLEEP_TYPE) {

                            for (int ii = 0; ii < (timeDelta / 1000 /60); ii++) {
                                graph += (timeDelta + ";");
                            }

                        }
                        //*/
                        //output.close();
                        if (sleepType != START_SLEEP_TYPE)
                            updateGraph(timeDelta);

                        deltaX = 0;
                        deltaY = 0;
                        deltaZ = 0;

                        //System.out.println("Accelerometer Reading!");

                    } else if (isExternalStorageReadable()) {

                    }


                } catch (IOException e) {
                    System.out.println(e.getMessage());
                }
                if (!lessThanTwoHoursUntilWakeUp) {
                    //if now until wake up is less than two hours
                    if (timeUntilWake() < 7200000) {
                        lessThanTwoHoursUntilWakeUp = true;
                        System.out.println("Less than two hours until wake up");
                    }
                }

                System.out.println((timeUntilWake()/1000/60)+" Minutes until wake.");

                //Identify Sleep Phase
                if (timeDelta < REM_MAX) {

                    if (sleepType != REM) {
                        types.add(sleepType);
                        durations.add(System.currentTimeMillis() - startOfPhaseTime);
                        startOfPhaseTime = System.currentTimeMillis();
                        phaseIndex++;
                        System.out.println("REM Detected");
                    }
                    sleepType = REM;

                    if(finalSleepCycle && !wakie) {
                        wakeUp();
                        wakie = true;
                    }
                    checkIfTimeForAnotherSleepCycle();

                } else if (timeDelta > SEMI_REM_MIN && timeDelta < SEMI_REM_MAX) {
                    if (sleepType != SEMI_REM) {
                        types.add(sleepType);
                        durations.add(System.currentTimeMillis() - startOfPhaseTime);
                        startOfPhaseTime = System.currentTimeMillis();
                        phaseIndex++;
                        System.out.println("SEMI-REM Detected");
                    }
                    sleepType = SEMI_REM;

                    if(finalSleepCycle) {
                        wakeUp();
                    }
                    checkIfTimeForAnotherSleepCycle();

                } else if (timeDelta > NON_REM_MIN) {
                    if (sleepType != NON_REM) {
                        types.add(sleepType);
                        durations.add(System.currentTimeMillis() - startOfPhaseTime);
                        startOfPhaseTime = System.currentTimeMillis();
                        phaseIndex++;
                        System.out.println("NON-REM Detected");
                        //Non rem sleep indicates a sleep cycle was just completed
                        if (phaseIndex != 1) { //Unless this is the first non rem sleep
                            //going back until we find the last NON-REM, and counting the amount of phases we went through
                            int HowManyPhasesTheCycleTook = 1;
                            int dur = 0;
                            while ((phaseIndex - HowManyPhasesTheCycleTook) > 0 ? (types.get(phaseIndex-HowManyPhasesTheCycleTook) != NON_REM) : false) {
                                dur += durations.get(phaseIndex-HowManyPhasesTheCycleTook);
                                HowManyPhasesTheCycleTook++;
                            }
                            //If there was not a sleep cycle yet, lastSleepCycle is 0, if there was, it is the total duration of all the phases.
                            lastSleepCycleDuration = phaseIndex - HowManyPhasesTheCycleTook == 0? 0:dur;
                            System.out.println("Last Sleep Cycle: "+(lastSleepCycleDuration/1000/60));
                        }
                    }
                    sleepType = NON_REM;

                } else {
                    System.out.println("DA FUQ?");
                    //sleepType=-1;
                }

            }

        }

    }

    private void updateGraph(long timeDelta) {
        pointIndex += (timeDelta/1000);
        graphPoints.add(new PointF(pointIndex, timeDelta));
        DataPoint [] data = new DataPoint[graphPoints.size()-1];
        for (int i=0;i<graphPoints.size()-1;i++)
            data[i] = new DataPoint(graphPoints.get(i).x,graphPoints.get(i).y);
        LineGraphSeries<DataPoint> series = new LineGraphSeries<>(data);
        series.setThickness(3);
        //series.setDrawDataPoints(true);
        //series.setTitle("Hi");
        MainActivity.gv.removeAllSeries();
        MainActivity.gv.addSeries(series);
    }

    private void wakeUp() {
        System.out.println("WAKE UP!!!!!!!!!!!!!!!!WAKE UP!!!!!!!!!!!!!");
        mp = MediaPlayer.create(MonitorSleep.this, R.raw.wakiewakie);
        mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

            @Override
            public void onCompletion(MediaPlayer mp) {
                // TODO Auto-generated method stub
                mp.start();
            }

        });
        mp.start();
    }

    private boolean checkIfTimeForAnotherSleepCycle() {
        if (lessThanTwoHoursUntilWakeUp) {
            if (timeUntilWake() >= (lastSleepCycleDuration + (lastSleepCycleDuration * SAFETY_MARGIN_PERCENT))) {
                if (this.phaseIndex != 0 && (Math.floor(timeUntilWake() / lastSleepCycleDuration) == 1))
                    finalSleepCycle=true;
                return true;
            } else if (timeUntilWake() < (lastSleepCycleDuration + SAFETY_MARGIN_PERCENT)){
                finalSleepCycle = true;
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }



    private void stopRecordSleep() {
        if (mp != null) {
            mp.stop();
            mp.release();
            mp = null;
        }
        wakie = false;
        preformDataAnalysis();
        finalSleepCycle = false;
        sleeping = false;
        lessThanTwoHoursUntilWakeUp = false;
        try {
            output.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        deltaX = 0;
        deltaY = 0;
        deltaZ = 0;

        lastX = 0;
        lastY = 0;
        lastZ = 0;

        this.lastSleepCycleDuration = 0;
        this.durations.clear();
        this.types.clear();
        this.phaseIndex = 0;
        this.wl.release();

    }

    private void preformDataAnalysis() {
        //Calculate Data
        for (int i=0;i<durations.size();i++) {
            if (types.get(i) == REM) {
                remTotal += durations.get(i);
                remCounter++;
                remDurations.add(durations.get(i));
            } else if (types.get(i) == SEMI_REM) {
                semiRemTotal += durations.get(i);
                semiRemCounter++;
                semiRemDurations.add(durations.get(i));
            } else if (types.get(i) == NON_REM) {
                nonRemTotal += durations.get(i);
                nonRemCounter++;
                nonRemDurations.add(durations.get(i));
            }

        }

        remAverage = remTotal / (remCounter!=0?remCounter:1);
        semiRemAverage = semiRemTotal / (semiRemCounter!=0?semiRemCounter:1);
        nonRemAverage = nonRemTotal / (nonRemCounter!=0?nonRemCounter:1);

        //makes the graph
        /*
        for (int i=0;i<durations.size();i++) {
            for (int ii=0;ii<(delta.get(i)/1000/60);ii++) {
                graph += (delta.get(i) + ";");
            }
        }
        */

        //Save File
        String t = "";
        String d = "";
        for (int i=0;i<durations.size();i++) {
            t += types.get(i)+";";
            d += (durations.get(i)/1000)+";";
        }

        String graphY = "";
        String graphX = "";
        for (int i=0;i<graphPoints.size();i++) {
            graphY += (graphPoints.get(i).y + ";");
            graphX += (graphPoints.get(i).x + ";");
        }

        String r = "";
        String s = "";
        String n = "";
        for (int i=0;i<remDurations.size();i++) {
            r += (remDurations.get(i)/1000)+";";
        }
        for (int i=0;i<semiRemDurations.size();i++) {
            s += (semiRemDurations.get(i)/1000)+";";
        }
        for (int i=0;i<nonRemDurations.size();i++) {
            n += ((nonRemDurations.get(i)/1000)/60f)+";";
        }
        try {
            output.newLine();
            output.newLine();
            output.newLine();

            output.write("Waking Hour:;" + wakingHour + ";Waking Minute:;" + wakingMinute);
            output.newLine();
            output.flush();

            output.write(t);
            output.newLine();
            output.flush();

            output.write(d);
            output.newLine();
            output.newLine();
            output.flush();

            output.write(graphY);
            output.newLine();
            output.flush();

            output.write(graphX);
            output.newLine();
            output.newLine();
            output.flush();

            output.write("REM durations:\n" + r + "\n");
            output.flush();

            output.write("SEMI-REM durations:\n" + s + "\n");
            output.flush();

            output.write("NON-REM durations:\n" + n + "\n\n");
            output.flush();

            output.write(";Counter;Average;Total");
            output.flush();
            output.write("\nREM (sec);"+(remCounter)+";"+(remAverage/1000)+";"+(remTotal/1000)+"\n");
            output.flush();
            output.write("\nSEMI REM (sec);"+(semiRemCounter)+";"+(semiRemAverage/1000)+";"+(semiRemTotal/1000)+"\n");
            output.flush();
            output.write("\nNON REM (Minutes);"+(nonRemCounter)+";"+((nonRemAverage/1000)/60)+";"+((nonRemTotal/1000)/60)+"\n");
            output.flush();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void recordSleep() {
        sleeping = true;
        timeStartRecording = System.currentTimeMillis();
        startHour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
        startMinute = Calendar.getInstance().get(Calendar.MINUTE);

        wl.acquire();

        //Create New file and directory if one does nto already exist
        try {
            if (isExternalStorageWritable()) {
                //*
                File dir = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "Sleep");

                if (!dir.exists()) {
                    dir.mkdir();
                }
                //*/

                File file = new File(dir.getAbsolutePath() + File.separator + "Sleep_Data_" + date + ".txt");


                output = new BufferedWriter(new FileWriter(file));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    //Utility Functions

    public int timeUntilWake() {
        calendar = Calendar.getInstance();
        int currentHour = calendar.get(Calendar.HOUR_OF_DAY);
        int currentMinute = calendar.get(Calendar.MINUTE);
        if (wakingHour > currentHour) {
            //This is during the day
            return (((wakingHour -currentHour)*60)   +   (wakingMinute -currentMinute))*60*1000;
        } else if (wakingHour < currentHour) {
            //This is over midnight, add 24 to wakingHour so "on the same page"
            return ((((wakingHour +24)-currentHour)*60)   +   (wakingMinute -currentMinute))*60*1000;
        } else {
            //Same wakingHour
            return (wakingMinute -currentMinute)*60*1000;
        }
    }

    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }
    /* Checks if external storage is available to at least read */
    public boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            return true;
        }
        return false;
    }



}
