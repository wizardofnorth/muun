package com.sleeping.brain.smartalarmclock;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Build;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Micheal on 12/7/2015.
 */
public class SleepGraph extends View {

    ArrayList<Integer> graph;

    public SleepGraph(Context context) {
        super(context);
    }

    public SleepGraph(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context,attrs);
    }

    public SleepGraph(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context,attrs);
    }

    public void init(Context context, AttributeSet attrs){
        graph = new ArrayList<>();
        /* No attributes
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.SleepGraph);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.sleep_list, this);



        final int N = a.getIndexCount();
        for (int i = 0; i < N; ++i)
        {
            int attr = a.getIndex(i);
            switch (attr)
            {
                case R.styleable.ButtonCatalogueElement_Picture:
                    int res = a.getResourceId(attr, 0);
                    setPicture(res);
                    break;
                case R.styleable.ButtonCatalogueElement_Description:
                    setDescription(a.getString(attr));
                    break;
            }
        }
        a.recycle();
        */
    }

    public void setGraph(String graphie) {
        graph.clear();
        boolean end = false;
        int currentSemiColon = 0;
        while (!end) {

            graph.add(
                    Integer.parseInt(
                            graphie.substring(
                                    currentSemiColon==0?currentSemiColon:currentSemiColon+1,
                                    graphie.indexOf(";", currentSemiColon+1)
                            )));
            currentSemiColon = graphie.indexOf(";", currentSemiColon+1);
            end = currentSemiColon==(graphie.length()-1);

        }
    }
    public void addGraphPoint(int point) {
        for (int ii = 0; ii < (point / 1000 /60); ii++) {
            graph.add(point);
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (graph.size() != 0) {
            final float WIDTH_PER_POINT = /*Math.floor(*/(getWidth() / graph.size())/*+0d)*/;

            int highestPoint = 0;
            for (int i = 0; i < graph.size(); i++) {
                if (graph.get(i) > highestPoint)
                    highestPoint = graph.get(i);
            }

            final float HEIGHT_PER_DEEPINESS_SLEEPINESS = getHeight() / highestPoint;

            Paint RegionPaint = new Paint();
            RegionPaint.setColor(Color.argb(128, 0, 0, 0));
            canvas.drawRect(0, 0, getWidth(), getHeight(), RegionPaint);

            RegionPaint.setColor(Color.argb(255, 0, 0, 128));
            canvas.drawRect(0, (MonitorSleep.SEMI_REM_MAX - 1) * HEIGHT_PER_DEEPINESS_SLEEPINESS, getWidth(), getHeight(), RegionPaint);

            RegionPaint.setColor(Color.argb(220, 76, 76, 166));
            canvas.drawRect(0, (MonitorSleep.REM_MAX - 1) * HEIGHT_PER_DEEPINESS_SLEEPINESS, getWidth(), getHeight(), RegionPaint);

            Paint lineGraphPaint = new Paint();
            lineGraphPaint.setColor(Color.argb(255, 107, 181, 255));
            //lineGraphPaint.setShadowLayer(3, 3, 3, Color.argb(255, 0, 0, 0));
            for (int i = 0; i < graph.size()-1; i++)
                canvas.drawLine(
                        i * WIDTH_PER_POINT,
                        graph.get(i) * HEIGHT_PER_DEEPINESS_SLEEPINESS,

                        (i + 1) * WIDTH_PER_POINT,
                        graph.get(i + 1) * HEIGHT_PER_DEEPINESS_SLEEPINESS,

                        lineGraphPaint);
        }
    }

}
