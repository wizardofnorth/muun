package com.sleeping.brain.smartalarmclock;


import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TimePicker;
import android.widget.ViewFlipper;

import com.jjoe64.graphview.DefaultLabelFormatter;
import com.jjoe64.graphview.GraphView;

import java.util.Calendar;

public class MainActivity extends ActionBarActivity {

    //For first view
    TimePicker timePicker;
    Calendar calendar;
    Switch alarmSwitch;
    Intent intent;

    //For View Flipping
    private ViewFlipper viewFlipper;
    private float lastX;

    //Listview
    ListView lv;
    public static GraphView gv;
    public int minuteStart = 0;
    public int hourStart = 0;

    @Override
    protected void onDestroy () {
        super.onDestroy();
        stopRecordSleep();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        calendar = Calendar.getInstance();


        timePicker = (TimePicker) findViewById(R.id.timePicker);
        timePicker.setBackgroundColor(Color.TRANSPARENT);
        timePicker.setIs24HourView(false);
        timePicker.setCurrentHour(PreferenceManager.getDefaultSharedPreferences(this).getInt("LastWakeHour", calendar.get(Calendar.HOUR_OF_DAY)));
        timePicker.setCurrentMinute(PreferenceManager.getDefaultSharedPreferences(this).getInt("LastWakeMinute", calendar.get(Calendar.MINUTE)));

        //lv = (ListView) findViewById(R.id.listView);
        //lv.setAdapter(new CustomAdapter(this, texts, g));
        gv = (GraphView) findViewById(R.id.graph);
        gv.setTitle("Now");
        gv.setTitleColor(Color.parseColor("#ff6bb5ff"));
        gv.getViewport().setScalable(true);
        gv.getViewport().setScrollable(true);


        // custom label formatter to show human readable time
        gv.getGridLabelRenderer().setLabelFormatter(new DefaultLabelFormatter() {
            @Override
            public String formatLabel(double value, boolean isValueX) {
                if (isValueX) {
                    // show normal x values
                    //ceil rounds down (DA FUQ?)
                    double additional_hours = Math.ceil(value / 60 / 60d);
                    double additional_minutes = Math.round(value/60f) % 60;

                    int h = (int) (additional_hours+MonitorSleep.startHour);
                    int m = (int) (additional_minutes+MonitorSleep.startMinute);

                    if (h>24)
                        h-=24;
                    //return super.formatLabel(value, isValueX);
                    return (h<10?"0"+h:h) + ":" + (m<10?"0"+m:m);
                } else {
                    // show normal y values
                    return super.formatLabel(value, isValueX);
                }
            }
        });

        viewFlipper = (ViewFlipper) findViewById(R.id.viewFlipper);

        alarmSwitch = (Switch) findViewById(R.id.alarmToggle);
        alarmSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    recordSleep();
                    //timePicker.setEnabled(false);

                } else if (!isChecked) {
                    stopRecordSleep();
                    //timePicker.setEnabled(true);
                }
            }
        });
    }

    private void stopRecordSleep() {
        if (intent != null) {
            stopService(intent);
        }
    }


    private void recordSleep() {

        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(this).edit();
        editor.putInt("LastWakeHour", timePicker.getCurrentHour());
        editor.putInt("LastWakeMinute", timePicker.getCurrentMinute());
        editor.apply();

        hourStart = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
        minuteStart = Calendar.getInstance().get(Calendar.MINUTE);

        intent = new Intent(getApplicationContext(), MonitorSleep.class);
        intent.putExtra("WakeHour", timePicker.getCurrentHour());
        intent.putExtra("WakeMinute", timePicker.getCurrentMinute());
        startService(intent);

        //Swipe screen to the side and show the live graph growing
        /*
        // Next screen comes in from left.
        viewFlipper.setInAnimation(this, R.anim.slide_in_from_left);

        // Current screen goes out from right.
        viewFlipper.setOutAnimation(this, R.anim.slide_out_to_right);

        // Display next screen.
        viewFlipper.showNext();
        */
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    // Using the following method, we will handle all screen swaps.
    public boolean onTouchEvent(MotionEvent touchevent) {

        switch (touchevent.getAction()) {

            case MotionEvent.ACTION_DOWN:

                lastX = touchevent.getX();
                break;
            case MotionEvent.ACTION_UP:

                float currentX = touchevent.getX();

                // Handling left to right screen swap.
                if (lastX < currentX) {
                    // If there aren't any other children, just break.
                    if (viewFlipper.getDisplayedChild() == 0)
                    break;

                    // Next screen comes in from left.
                    viewFlipper.setInAnimation(this, R.anim.slide_in_from_left);

                    // Current screen goes out from right.
                    viewFlipper.setOutAnimation(this, R.anim.slide_out_to_right);

                    // Display next screen.
                    viewFlipper.showNext();

                }

                // Handling right to left screen swap.
                if (lastX > currentX) {
                    // If there is a child (to the left), kust break.
                    if (viewFlipper.getDisplayedChild() == 1)
                        break;

                    // Next screen comes in from right.
                    viewFlipper.setInAnimation(this, R.anim.slide_in_from_right);

                    // Current screen goes out from left.
                    viewFlipper.setOutAnimation(this, R.anim.slide_out_to_left);
                    // Display previous screen.
                    viewFlipper.showPrevious();
                }
                break;
        }
        return false;

    }


}
